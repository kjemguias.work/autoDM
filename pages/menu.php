<?php

	require('../scripts/connect.php');

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Neon Admin Panel" />
	<meta name="author" content="" />

	<title>Neon | Register</title>

	<link rel="stylesheet" href="../assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="../assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="../assets/css/bootstrap.css">
	<link rel="stylesheet" href="../assets/css/neon-core.css">
	<link rel="stylesheet" href="../assets/css/neon-theme.css">
	<link rel="stylesheet" href="../assets/css/neon-forms.css">
	<link rel="stylesheet" href="../assets/css/custom.css">

	<script src="../assets/js/jquery-1.11.0.min.js"></script>
	<script>$.noConflict();</script>

	<!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body class="page-body login-page login-form-fall" data-url="http://neon.dev">


<!-- This is needed when you send requests via Ajax -->
<script type="text/javascript">
var baseurl = '';
</script>

<div class="login-container">

	<div class="login-header login-caret">

		<div class="login-content">

			<a href="#" class="logo" data-toggle="dropdown ">
				<img src="../assets/images/logo1.png" width="120" alt="" />
			</a>

			<p class="description">Welcome <span class="badge badge-success"><b><u><?php echo strtoupper($_SESSION["user"]);?></u></b></span> to Auto Duty Manager for Snow</p>

			<!-- progress bar indicator -->
			<div class="login-progressbar-indicator">
				<h3>43%</h3>
				<span>Logging in snow...</span>
			</div>



		</div>

	</div>

	<div class="login-progressbar">
		<div></div>
	</div>

	<div class="login-form">

		<div class="col col-md-4 col-md-offset-4">

			<h1 class="text-primary"><center><u>MAIN MENU</u></center></h1>
			<br/>
			<div class="row">
					<div class="col col-md-12">
						<div class="form-group">
							<a href="#" class="btn btn-block btn-primary btn-login">
								<i class="entypo-rocket"></i>
								<center>Auto Duty Manager</center>
							</a>
						</div>
					</div>
			</div>

			<div class="row">
					<div class="col col-md-6">
						<div class="form-group">
							<a href="leaves_meetings.php" class="btn btn-block btn-primary btn-login">
								<i class="entypo-suitcase"></i>
								Leaves & Meetings
							</a>
						</div>
					</div>

					<div class="col col-md-6">
						<div class="form-group">
							<a href="dm_sm.php" class="btn btn-block btn-primary btn-login">
								<i class="entypo-flow-parallel"></i>
								DM's and SM's
							</a>
						</div>
					</div>
			</div>

			<div class="row">
					<div class="col col-md-4">
						<div class="form-group">
							<a href="teams.php" class="btn btn-block btn-primary btn-login">
								<i class="entypo-google-circles"></i>
								Teams
							</a>
						</div>
					</div>

					<div class="col col-md-4">
						<div class="form-group">
							<a href="regions.php" class="btn btn-block btn-primary btn-login">
								<i class="entypo-globe"></i>
								Regions
							</a>
						</div>
					</div>

					<div class="col col-md-4">
						<div class="form-group">
							<a href="rows.php" class="btn btn-block btn-primary btn-login">
								<i class="entypo-menu"></i>
								Rows
							</a>
						</div>
					</div>
			</div>
			<div class="row">

				<div class="col col-md-6">
					<div class="form-group">
						<a href="levels.php" class="btn btn-block btn-primary btn-login">
							<i class="entypo-sweden"></i>
							<center>Levels</center>
						</a>
					</div>
				</div>

					<div class="col col-md-6">
						<div class="form-group">
							<a href="#" class="btn btn-block btn-primary btn-login">
								<i class="entypo-users"></i>
								<center>Specialist</center>
							</a>
						</div>
					</div>
			</div>

			<div class="row">
					<div class="login-bottom-links">
							<center>
									<a href="../scripts/logout.php" class="link">
											<i class="entypo-lock"></i>
											Logout
									</a>
									<br />
							</center>
						</div>
			</div>
		</div>
	</div>

</div>

	<script src="../scripts/table-script.js"></script>
  	<link rel="stylesheet" href="../assets/js/datatables/responsive/css/datatables.responsive.css">
  	<link rel="stylesheet" href="../assets/js/select2/select2-bootstrap.css">
  	<link rel="stylesheet" href="../assets/js/select2/select2.css">

	<!-- Bottom scripts (common) -->
	<script src="../assets/js/gsap/main-gsap.js"></script>
	<script src="../assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="../assets/js/bootstrap.js"></script>
	<script src="../assets/js/joinable.js"></script>
	<script src="../assets/js/resizeable.js"></script>
	<script src="../assets/js/neon-api.js"></script>
	<script src="../assets/js/jquery.validate.min.js"></script>
	<script src="../assets/js/neon-register.js"></script>
	<script src="../assets/js/jquery.inputmask.bundle.min.js"></script>
	<script src="../assets/js/jquery.dataTables.min.js"></script>
  	<script src="../assets/js/datatables/TableTools.min.js"></script>




	<!-- JavaScripts initializations and stuff -->
	<script src="../assets/js/dataTables.bootstrap.js"></script>
  <script src="../assets/js/datatables/jquery.dataTables.columnFilter.js"></script>
  <script src="../assets/js/datatables/lodash.min.js"></script>
  <script src="../assets/js/datatables/responsive/js/datatables.responsive.js"></script>
  <script src="../assets/js/select2/select2.min.js"></script>
	<script src="../assets/js/neon-custom.js"></script>
	<script src="../assets/js/neon-mail.js"></script>


	<!-- Demo Settings -->
	<script src="../assets/js/neon-demo.js"></script>

</body>
</html>

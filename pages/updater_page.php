<?php 

	require('../scripts/connect.php');
	
	if(empty($_SESSION['user']))
	{
		header("Location: ../");
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Neon Admin Panel" />
	<meta name="author" content="" />

	<title>Neon | Tocify</title>

	<link rel="stylesheet" href="../assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="../assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="../assets/css/font-icons/font-awesome/css/font-awesome.css">
	<link rel="stylesheet" href="../assets/css/bootstrap.css">
	<link rel="stylesheet" href="../assets/css/neon-core.css">
	<link rel="stylesheet" href="../assets/css/neon-theme.css">
	<link rel="stylesheet" href="../assets/css/neon-forms.css">
	<link rel="stylesheet" href="../assets/css/custom.css">

	<script src="../assets/js/jquery-1.11.0.min.js"></script>
	<script>$.noConflict();</script>

	<!--[if lt IE 9]><script src="../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->


</head>
<body class="page-body" onload="checker()">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	<div class="main-content">
				
		<div class="row">
		
			<!-- Profile Info and Notifications -->
			<div class="col-md-6 col-sm-8 clearfix">
		
				<ul class="user-info pull-left pull-none-xsm">
		
					<!-- Profile Info -->
					<li class="profile-info dropdown"><!-- add class "pull-right" if you want to place this from right -->
		
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<img src="../assets/images/logo2.png" alt=""  width="44" />
							Auto<strong>DM</strong>
						</a>
		
						<ul class="dropdown-menu">
		
							<!-- Reverse Caret -->
							<li class="caret"></li>
		
							<!-- Profile sub-links -->
							<li>
								<a href="landing_page.php">
									<i class="fa-adn"></i>
									Home
								</a>
							</li>

							<li>
								
								<a href="users.php">
									<i class="entypo-user"></i>
									View/Edit Specialist
								</a>
							</li>
		
							<li>
								<a href="#">
									<i class="entypo-google-circles"></i>
									View/Edit Rows
								</a>
							</li>
		
							<li>
								<a href="#">
									<i class="entypo-calendar"></i>
									Leaves and Meetings
								</a>
							</li>
						</ul>
					</li>
		
				</ul>

			</div>
		
		
			<!-- Raw Links -->
			<div class="col-md-6 col-sm-4 clearfix hidden-xs">
		
				<ul class="list-inline links-list pull-right">		
					<li>
						<a href="../scripts/logout.php">
							Log Out <i class="entypo-logout right"></i>
						</a>
					</li>
				</ul>
		
			</div>
		
		</div>
		
		<hr />
		
		<div class="row">
			<div class="col col-md-12">
				<div class="tab-content">
					<div class="tab-pane active" id="view">
						<h3>Ticket Updater</h3>
		                <hr/>
		                <div class="row">
			                <div class="col-md-6">
			                	<form method="post" action="../scripts/updater_auto.php">
			                		<div class="panel panel-default">
			                			<div class="panel panel-heading">
			                				<div class="panel-title">
							                	<label> Ticket Details </label>
							                </div>
							            </div>
							            <div class="panel-body">
								                <div class="form-group">
								                	<label>Enter Ticket Number:</label>
								                	<textarea name="ticketNumber" class="form-control autogrow" id="field-ta" placeholder="Enter ticket number here separated with line breaks" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 82px;" required></textarea>
								                </div>

								                <div class="form-group">
								                	<label> Workgroup </label>
								                	<input type="text" class="form-control" name="workgroup" placeholder="Enter Workgroup" required>
								                </div>

								                <div class="form-group">
								                	<label> Assignee </label>
								                	<input type="text" class="form-control" name="assignee" placeholder="Enter Assignee" required>
								                </div>

								                <div class="form-group">
								                	<label> Master ticket </label>
								                	<input type="text" class="form-control" name="master" placeholder="Enter Master Ticket #" required>
								                </div>
								        </div>
								        <div class="panel-footer">
							                <input type="submit" class="btn btn-default btn-block">
							            </div>
								    </div>
							    </div>
							    </form>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>

		</div>
		<!-- Footer -->
		<footer class="main">
			
			&copy; 2014 <strong>Neon</strong> Admin Theme by <a href="http://laborator.co" target="_blank">Laborator</a>
		
		</footer>
	</div>

		
	<script type="text/javascript">

		function checker()
		{
				var opts = {
				"closeButton": true,
				"debug": false,
				"positionClass": "toast-top-full-width",
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "5000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
			};

			var url_string = window.location.href;
			var url = new URL(url_string);
			var c = url.searchParams.get("success");
			
			if(c)
			{
				toastr.success("Successfully closed the tickets", "Ticker Update", opts);
			}
			else
			{
				toastr.error("An error occured when closing the tickets", "Ticker Update", opts);
			}
		}
		

	</script>




	<!-- Bottom scripts (common) -->
	<script src="../assets/js/gsap/main-gsap.js"></script>
	<script src="../assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="../assets/js/bootstrap.js"></script>
	<script src="../assets/js/joinable.js"></script>
	<script src="../assets/js/resizeable.js"></script>
	<script src="../assets/js/neon-api.js"></script>
	<script src="../assets/js/toastr.js"></script>


	<!-- Imported scripts on this page -->
	<script src="../assets/js/tocify/jquery.tocify.min.js"></script>
	<script src="../assets/js/neon-chat.js"></script>


	<!-- JavaScripts initializations and stuff -->
	<script src="../assets/js/neon-custom.js"></script>


	<!-- Demo Settings -->
	<script src="../assets/js/neon-demo.js"></script>

</body>
</html>
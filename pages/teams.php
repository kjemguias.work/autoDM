<?php

	require('../scripts/connect.php');

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Neon Admin Panel" />
	<meta name="author" content="" />

	<title>Neon | Register</title>

	<link rel="stylesheet" href="../assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="../assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="../assets/css/bootstrap.css">
	<link rel="stylesheet" href="../assets/css/neon-core.css">
	<link rel="stylesheet" href="../assets/css/neon-theme.css">
	<link rel="stylesheet" href="../assets/css/neon-forms.css">
	<link rel="stylesheet" href="../assets/css/custom.css">

	<script src="../assets/js/jquery-1.11.0.min.js"></script>
	<script>$.noConflict();</script>

	<!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body class="page-body login-page login-form-fall" data-url="http://neon.dev">


<!-- This is needed when you send requests via Ajax -->
<script type="text/javascript">
var baseurl = '';
</script>

<div class="login-container">

	<div class="login-header login-caret">

		<div class="login-content">

			<a href="#" class="logo" data-toggle="dropdown ">
				<img src="../assets/images/logo1.png" width="120" alt="" />
			</a>

			<p class="description">Welcome <span class="badge badge-success"><b><u><?php echo strtoupper($_SESSION["user"]);?></u></b></span> to Auto Duty Manager for Snow</p>

			<!-- progress bar indicator -->
			<div class="login-progressbar-indicator">
				<h3>43%</h3>
				<span>Logging in snow...</span>
			</div>



		</div>

	</div>

	<div class="login-progressbar">
		<div></div>
	</div>

	<div class="login-form">

		<div class="col col-md-8 col-md-offset-2">
				<br>
				<div class="form-steps">
					<div class="step current" id="step-2">

					<blockquote>
						<p>
						<strong>Teams Management</strong>
						<br>
						View Current setup Teams
						<br>
						<hr/>
						See below setup teams and details.
						<br/>
						'Below Delete / Add team is currently disabled for data protection (please contact administrator to enable the function or update the teams)'
						</p>
					</blockquote>

					<table class="table table-bordered datatable" id="table-1">
                      <thead>
                        <tr class="replace-inputs">
                          <th style="width: 10px">ID</th>
                          <th>Team Name</th>
													<th>Description</th>
													<th style="width: 10px">Update</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php

                          $selectSQL = mysqli_query($conn, "SELECT * from team");

                          while($row = mysqli_fetch_array($selectSQL))
                          {
                            ?>
                            <tr>
                              <form action="../scripts/remove_dm_sm.php" method="post">
                              <td><?php echo $row['ID'];?> <input name="specialist_id" type="text" value="<?php echo $row['ID']; ?>" hidden></td>
                              <td><?php echo $row['team_name'];?></td>
                              <td><?php echo $row['description'];?></td>
                              <td>
                              		<button disabled type="submit" class="btn btn-block btn-white">
																	<i class="entypo-trash"></i>
														  		</button>
														  </td>
														  </form>
                            </tr>
                            <?php
	                          	}
	                          ?>
                      </tbody>
                      <tfoot>
                        <tr>
													<th style="width: 10px">ID</th>
                          <th>Team Name</th>
													<th>Description</th>
													<th>Update</th>
                        </tr>
                      </tfoot>
                    </table>

						<div class="form-group">
							<a href="menu.php" class="btn btn-primary btn-login">
								<i class="entypo-left-open-mini"></i>
								Back to Main Menu
							</a>
							<a disabled href="javascript:;" onclick="jQuery('#modal-6').modal('show', {backdrop: 'static'});" class="btn btn-success btn-login pull-right">
								Add Team <i class="entypo-plus"></i></a>

						</div>

						<!--<div class="form-group">
							<button type="submit" class="btn btn-success btn-block btn-login">
								<i class="entypo-right-open-mini"></i>
								Complete Registration
							</button>
						</div>-->
					</div>

				</div>

			<div class="login-bottom-links"><center>

				<a href="../scripts/logout.php" class="link">
					<i class="entypo-lock"></i>
					Logout
				</a>
				<br />
			</center>
			</div>

		</div>
	</div>

</div>


	<!-- MODAL -->

	<div class="modal fade" id="modal-6">
		<div class="modal-dialog">
			<div class="modal-content">

			<?php

			$selectSQL = mysqli_query($conn, "SELECT * FROM `specialist` left join specialist_details on specialist.ID = specialist_details.specialist_id where role_id = 0;");
			$selectRoleSQL = mysqli_query($conn, "SELECT * FROM support_role");

			?>

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Set DM's and SM's</h4>
				</div>
				<form action="../scripts/add_dm_sm.php" method="POST" role="form">
				<div class="modal-body">

					<div class="row">
						<div class="col-md-12">
							<?php
							if(mysqli_num_rows($selectSQL) == 0){

							    echo "There are no available specialist to set as DM and SMs (possible scenario is they're already set as DM's and SM's)";
							    $counter = 1;
							}
							else
							{
								$counter = 0;
							?>

							<div class="form-group">
								<label class="col-sm-12 control-label">Specialist</label>
								<div class="col-sm-12">
									<select name="specialist" class="select2" data-allow-clear="true" data-placeholder="Select Specialist">
										<option></option>
										<?php
										while($row = mysqli_fetch_array($selectSQL))
                         				{
                         				?>
											<option value="<?php echo $row['ID'];?>"><?php echo $row['specialist_name'];?></option>
										<?php
										}
										?>
									</select>

								</div>
							</div>


							<div class="form-group">
								<label class="col-sm-12 control-label">Role</label>
								<div class="col-sm-12">
									<select name="role" class="select2" data-allow-clear="true" data-placeholder="Select Specialist Role">
										<option></option>
										<?php
										while($row = mysqli_fetch_array($selectRoleSQL))
                         				{
                         				?>
											<option value="<?php echo $row['ID'];?>"><?php echo $row['role'];?></option>
										<?php
										}
										?>
									</select>
								</div>
							</div>
							<?php }?>
						</div>

				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<?php if($counter == 0){?>
					<button type="submit" class="btn btn-info">Save changes</button>
					<?php }?>
				</div>
				</form>
			</div>
		</div>
	</div>

	<!--       -->

	<script src="../scripts/table-script.js"></script>
  	<link rel="stylesheet" href="../assets/js/datatables/responsive/css/datatables.responsive.css">
  	<link rel="stylesheet" href="../assets/js/select2/select2-bootstrap.css">
  	<link rel="stylesheet" href="../assets/js/select2/select2.css">

	<!-- Bottom scripts (common) -->
	<script src="../assets/js/gsap/main-gsap.js"></script>
	<script src="../assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="../assets/js/bootstrap.js"></script>
	<script src="../assets/js/joinable.js"></script>
	<script src="../assets/js/resizeable.js"></script>
	<script src="../assets/js/neon-api.js"></script>
	<script src="../assets/js/jquery.validate.min.js"></script>
	<script src="../assets/js/neon-register.js"></script>
	<script src="../assets/js/jquery.inputmask.bundle.min.js"></script>
	<script src="../assets/js/jquery.dataTables.min.js"></script>
  	<script src="../assets/js/datatables/TableTools.min.js"></script>




	<!-- JavaScripts initializations and stuff -->
	<script src="../assets/js/dataTables.bootstrap.js"></script>
  <script src="../assets/js/datatables/jquery.dataTables.columnFilter.js"></script>
  <script src="../assets/js/datatables/lodash.min.js"></script>
  <script src="../assets/js/datatables/responsive/js/datatables.responsive.js"></script>
  <script src="../assets/js/select2/select2.min.js"></script>
	<script src="../assets/js/neon-custom.js"></script>
	<script src="../assets/js/neon-mail.js"></script>


	<!-- Demo Settings -->
	<script src="../assets/js/neon-demo.js"></script>

</body>
</html>

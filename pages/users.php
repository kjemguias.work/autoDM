<?php 

	require('../scripts/connect.php');
	
	if(empty($_SESSION['user']))
	{
		header("Location: ../");
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Neon Admin Panel" />
	<meta name="author" content="" />

	<title>autoDM | Users</title>

	<link rel="stylesheet" href="../assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="../assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="../assets/css/font-icons/font-awesome/css/font-awesome.css">
	<link rel="stylesheet" href="../assets/css/bootstrap.css">
	<link rel="stylesheet" href="../assets/css/neon-core.css">
	<link rel="stylesheet" href="../assets/css/neon-theme.css">
	<link rel="stylesheet" href="../assets/css/neon-forms.css">
	<link rel="stylesheet" href="../assets/css/custom.css">

	<script src="../assets/js/jquery-1.11.0.min.js"></script>
	<script>$.noConflict();</script>

	<!--[if lt IE 9]><script src="../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

	<style>

	.contain {
	    position: relative;
	    width: 100%;
	    height: 0;
	    padding-bottom: 45%;
	}

	.contain2 {
	    position: relative;
	    width: 100%;
	    height: 0;
	    padding-bottom: 30%;
	}

	.frame {
	    position: absolute;
	    top: 0;
	    left: 0;
	    width: 100%;
	    height: 100%;
	}

	</style>


</head>
<body class="page-body" data-url="http://neon.dev">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	<div class="main-content">
				
		<div class="row">
		
			<!-- Profile Info and Notifications -->
			<div class="col-md-6 col-sm-8 clearfix">
		
				<ul class="user-info pull-left pull-none-xsm">
		
					<!-- Profile Info -->
					<li class="profile-info dropdown"><!-- add class "pull-right" if you want to place this from right -->
		
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<img src="../assets/images/logo2.png" alt=""  width="44" />
							Auto<strong>DM</strong>
						</a>
		
						<ul class="dropdown-menu">
		
							<!-- Reverse Caret -->
							<li class="caret"></li>
		
							<!-- Profile sub-links -->
							<li>
								<a href="landing_page.php">
									<i class="fa fa-adn"></i>
									Home
								</a>
							</li>

							<li>
								<a href="#">
									<i class="entypo-user"></i>
									View/Edit Specialist
								</a>
							</li>
		
							<li>
								<a href="#">
									<i class="entypo-google-circles"></i>
									View/Edit Rows
								</a>
							</li>
		
							<li>
								<a href="#">
									<i class="entypo-calendar"></i>
									Leaves and Meetings
								</a>
							</li>
						</ul>
					</li>
		
				</ul>

			</div>
		
		
			<!-- Raw Links -->
			<div class="col-md-6 col-sm-4 clearfix hidden-xs">
		
				<ul class="list-inline links-list pull-right">		
					<li>
						<a href="../scripts/logout.php">
							Log Out <i class="entypo-logout right"></i>
						</a>
					</li>
				</ul>
		
			</div>
		
		</div>
		
		<hr />
		
		<div class="row">
			<div class="col-md-12">
				
				<!-- BODY -->

				<h3>Specialist Menu</h3>
				
				<ul class="nav nav-tabs bordered"><!-- available classes "bordered", "right-aligned" -->
					<li class="active">
						<a href="#view" data-toggle="tab">
							<span class="visible-xs"><i class="entypo-home"></i></span>
							<span class="hidden-xs">View</span>
						</a>
					</li>
					<li>
						<a href="#add" data-toggle="tab">
							<span class="visible-xs"><i class="entypo-user"></i></span>
							<span class="hidden-xs">Add</span>
						</a>
					</li>
					<li>
						<a href="#edit" data-toggle="tab">
							<span class="visible-xs"><i class="entypo-mail"></i></span>
							<span class="hidden-xs">Edit</span>
						</a>
					</li>
					<li>
					</li>
				</ul>
				
				<div class="tab-content">
					<div class="tab-pane active" id="view">
						<div class="contain">
							<iframe id="i-view" class="frame" frameborder="0" scrolling="no" src="iframe/view-user.php"></iframe>
						</div>
					</div>
					<div class="tab-pane" id="add">
						<div class="contain2">
							<iframe id="i-add" class="frame" frameborder="0" scrolling="no" src="iframe/add-user.php"></iframe>
						</div>	
					</div>

					<div class="tab-pane" id="edit">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									Specialist
									<select class="form-control" id="team" name="team">
									<?php 

										$specialistSelectSQL = mysqli_query($conn, "SELECT * from Specialist");

										while($specialistRow = mysqli_fetch_array($specialistSelectSQL))
										{
									?>
										<option value="<?php echo $specialistRow['ID']; ?>"><?php echo $specialistRow['specialist_name'];?></option>
									<?php }?>
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- BODY -->

			</div>
		</div>
		<!-- Footer -->
		<footer class="main">
			
			&copy; 2017 - Auto<strong>DM</strong> by <a href="#">FSS L1</a>
		
		</footer>
	</div>

	<!-- Bottom scripts (common) -->
	<script src="../assets/js/gsap/main-gsap.js"></script>
	<script src="../assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="../assets/js/bootstrap.js"></script>
	<script src="../assets/js/joinable.js"></script>
	<script src="../assets/js/resizeable.js"></script>
	<script src="../assets/js/neon-api.js"></script>


	<!-- Imported scripts on this page -->
	<script src="../assets/js/tocify/jquery.tocify.min.js"></script>
	<script src="../assets/js/neon-chat.js"></script>


	<!-- JavaScripts initializations and stuff -->
	<script src="../assets/js/neon-custom.js"></script>


	<!-- Demo Settings -->
	<script src="../assets/js/neon-demo.js"></script>

</body>
</html>
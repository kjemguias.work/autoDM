<html>
<head>
	<title>TEST</title>

	<link rel="stylesheet" href="../scripts/main.css">

	<script src="../assets/js/jquery-1.11.0.min.js"></script>
	<script>$.noConflict();</script>

</head>
<BODY>
	<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
		<div class="main-content">
				
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						Enter
						<input type="text" class="form-control input-sm" name="email" id="email" placeholder="Enter details" onkeyup="showHint(this.value)">
					</div>
					<div class="form-group">
						Suggestion: <span id="txtHint"></span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Bottom scripts (common) -->
	<link rel="stylesheet" href="../assets/js/datatables/responsive/css/datatables.responsive.css">
	<link rel="stylesheet" href="../assets/js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="../assets/js/select2/select2.css">
	<script src="../assets/js/gsap/main-gsap.js"></script>
	<script src="../assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="../assets/js/bootstrap.js"></script>
	<script src="../assets/js/joinable.js"></script>
	<script src="../assets/js/resizeable.js"></script>
	<script src="../assets/js/neon-api.js"></script>
	<script src="../scripts/ajax-test.js"></script>


	<!-- Imported scripts on this page -->
	<script src="../assets/js/tocify/jquery.tocify.min.js"></script>
	<script src="../assets/js/neon-chat.js"></script>


	<!-- JavaScripts initializations and stuff -->
	<script src="../assets/js/neon-custom.js"></script>


	<!-- Demo Settings -->
	<script src="../assets/js/neon-demo.js"></script>

</BODY>
</html>
<?php 

	require('../scripts/connect.php');
	
	if(empty($_SESSION['user']))
	{
		header("Location: ../");
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Neon Admin Panel" />
	<meta name="author" content="" />

	<title>autoDM | Users</title>

	<link rel="stylesheet" href="../scripts/main.css">
<!--  -->
	<script src="../assets/js/jquery-2.1.3.js"></script>
	<script>$.noConflict();</script>

	<!--[if lt IE 9]><script src="../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


</head>
<body class="page-body" data-url="http://neon.dev">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	<div class="main-content">
				
		<div class="row">
		
			<!-- Profile Info and Notifications -->
			<div class="col-md-6 col-sm-8 clearfix">
		
				<ul class="user-info pull-left pull-none-xsm">
		
					<!-- Profile Info -->
					<li class="profile-info dropdown"><!-- add class "pull-right" if you want to place this from right -->
		
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<img src="../assets/images/logo2.png" alt=""  width="44" />
							Auto<strong>DM</strong>
						</a>
		
						<ul class="dropdown-menu">
		
							<!-- Reverse Caret -->
							<li class="caret"></li>
		
							<!-- Profile sub-links -->
							<li>
								<a href="landing_page.php">
									<i class="fa fa-adn"></i>
									Home
								</a>
							</li>

							<li>
								<a href="#">
									<i class="entypo-user"></i>
									View/Edit Specialist
								</a>
							</li>
		
							<li>
								<a href="#">
									<i class="entypo-google-circles"></i>
									View/Edit Rows
								</a>
							</li>
		
							<li>
								<a href="#">
									<i class="entypo-calendar"></i>
									Leaves and Meetings
								</a>
							</li>
						</ul>
					</li>
		
				</ul>

			</div>
		
		
			<!-- Raw Links -->
			<div class="col-md-6 col-sm-4 clearfix hidden-xs">
		
				<ul class="list-inline links-list pull-right">		
					<li>
						<a href="../scripts/logout.php">
							Log Out <i class="entypo-logout right"></i>
						</a>
					</li>
				</ul>
		
			</div>
		
		</div>
		
		<hr />
		
		<div class="row">
			<div class="col-md-12">
				
				<!-- BODY -->

				<h3>Rows Menu</h3>
				
				<ul class="nav nav-tabs bordered"><!-- available classes "bordered", "right-aligned" -->
					<li class="active">
						<a href="#view" data-toggle="tab">
							<span class="visible-xs"><i class="entypo-home"></i></span>
							<span class="hidden-xs">View</span>
						</a>
					</li>
					<li>
						<a href="#add" data-toggle="tab">
							<span class="visible-xs"><i class="entypo-user"></i></span>
							<span class="hidden-xs">Add</span>
						</a>
					</li>
					<li>
						<a href="#edit" data-toggle="tab">
							<span class="visible-xs"><i class="entypo-mail"></i></span>
							<span class="hidden-xs">Edit</span>
						</a>
					</li>
					<li>
					</li>
				</ul>
				
				<div class="tab-content">
						<div class="tab-pane active" id="view">
							<h3>Rows List</h3>
	                  		<hr/>
	                  		<div class="row">
		                  		<div class="col-md-6">
					                <table class="table table-bordered datatable" id="table-1">
					                    <thead>
					                        <tr class="replace-inputs">
					                          <th>ID</th>
					                          <th>Row</th>
					                          <th>Description</th>
					                        </tr>
					                    </thead>
					                    <tbody> 
					                      		<?php 

					                      			$selectSQL = mysqli_query($conn, "SELECT * FROM support_rows");

					                      			while($row = mysqli_fetch_array($selectSQL))
							                        {
							                    ?>
							                    	<tr>
							                    	<td><?php echo $row['ID'];?></td>
					                                <td><?php echo $row['row'];?></td>
					                                <td><?php echo $row['description'];?></td>
					                                </tr>
							                    <?php }?>
					                      </tbody>
					                      <tfoot>
					                        <tr>
					                          <th>ID</th>
					                          <th>Row</th>
					                          <th>Description</th>
					                        </tr>
					                      </tfoot>
					                </table>
					            </div>
					            <div class="col-md-6">
					            	<div class="row">
					            		<div class="col-md-12">
											<div class="tile-block tile-green">
												<form id="rowForm" method="post" role="form" >
													<div class="tile-header">
														<i class="glyphicon glyphicon-align-justify"></i>
														
														<a href="#">
															Rows
															<span>FSS supported Rows!</span>
														</a>
													</div>

													<div class="tile-content">
														<div class="form-group">
															<strong>New Row</strong>
														</div>
														<div class="form-group">
															<input name="rowName" type="text" class="form-control" placeholder="Enter new Row"/>
														</div>
														<div class="form-group">
															<input name="description" type="text" class="form-control" placeholder="Enter Row Description"/>
														</div>
													</div>
													<button type="submit" class="btn btn-block tile-footer">ADD NEW ROW</button>
												</form>
											</div>
											
										</div>
					            	</div>
					            	<div class="row">
					            	</div>
					            </div>
					        </div>
						</div>
					<div class="tab-pane" id="add">
						<div class="contain2">
							<iframe id="i-add" class="frame" frameborder="0" scrolling="no" src="iframe/add-user.php"></iframe>
						</div>	
					</div>

					<div class="tab-pane" id="edit">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									Rows
									<select class="form-control" id="team" name="team">
									<?php 

										$rowsSelectSQL = mysqli_query($conn, "SELECT * from support_rows");

										while($rowsRow = mysqli_fetch_array($rowsSelectSQL))
										{
									?>
										<option value="<?php echo $rowsRow['ID']; ?>"><?php echo $rowsRow['row'] . " - " . $rowsRow['description'];?></option>
									<?php }?>
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- BODY -->

			</div>
		</div>
		<!-- Footer -->
		<footer class="main">
			
			&copy; 2017 - Auto<strong>DM</strong> by <a href="#">FSS L1</a>
		
		</footer>
	</div>



  <script src="../scripts/table-script.js"></script>
  <script src="../scripts/row-add.js"></script>
	<!-- Bottom scripts (common) -->
  <link rel="stylesheet" href="../assets/js/datatables/responsive/css/datatables.responsive.css">
  <link rel="stylesheet" href="../assets/js/select2/select2-bootstrap.css">
  <link rel="stylesheet" href="../assets/js/select2/select2.css">

  <!-- Bottom scripts (common) -->
  <script src="../assets/js/gsap/main-gsap.js"></script>

  <script src="../assets/js/jquery-ui/js/jquery-ui-2.1.3.js"></script>
  <script src="../assets/js/bootstrap.js"></script>
  <script src="../assets/js/joinable.js"></script>
  <script src="../assets/js/resizeable.js"></script>
  <script src="../assets/js/neon-api.js"></script>
  <script src="../assets/js/jquery.dataTables.min.js"></script>
  <script src="../assets/js/datatables/TableTools.min.js"></script>


  <!-- Imported scripts on this page -->
  <script src="../assets/js/dataTables.bootstrap.js"></script>
  <script src="../assets/js/datatables/jquery.dataTables.columnFilter.js"></script>
  <script src="../assets/js/datatables/lodash.min.js"></script>
  <script src="../assets/js/datatables/responsive/js/datatables.responsive.js"></script>
  <script src="../assets/js/select2/select2.min.js"></script>
  <script src="../assets/js/neon-chat.js"></script>


  <!-- JavaScripts initializations and stuff -->
  <script src="../assets/js/neon-custom.js"></script>


  <!-- Demo Settings -->
  <script src="../assets/js/neon-demo.js"></script>

</body>
</html>
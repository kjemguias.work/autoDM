<?php 

	require('../scripts/connect.php');
	
	if(empty($_SESSION['user']))
	{
		header("Location: ../");
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Neon Admin Panel" />
	<meta name="author" content="" />

	<title>Neon | Tocify</title>

	<link rel="stylesheet" href="../assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="../assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="../assets/css/font-icons/font-awesome/css/font-awesome.css">
	<link rel="stylesheet" href="../assets/css/bootstrap.css">
	<link rel="stylesheet" href="../assets/css/neon-core.css">
	<link rel="stylesheet" href="../assets/css/neon-theme.css">
	<link rel="stylesheet" href="../assets/css/neon-forms.css">
	<link rel="stylesheet" href="../assets/css/custom.css">

	<script src="../assets/js/jquery-1.11.0.min.js"></script>
	<script>$.noConflict();</script>

	<!--[if lt IE 9]><script src="../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->


</head>
<body class="page-body" data-url="http://neon.dev">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	<div class="main-content">
				
		<div class="row">
		
			<!-- Profile Info and Notifications -->
			<div class="col-md-6 col-sm-8 clearfix">
		
				<ul class="user-info pull-left pull-none-xsm">
		
					<!-- Profile Info -->
					<li class="profile-info dropdown"><!-- add class "pull-right" if you want to place this from right -->
		
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<img src="../assets/images/logo2.png" alt=""  width="44" />
							Auto<strong>DM</strong>
						</a>
		
						<ul class="dropdown-menu">
		
							<!-- Reverse Caret -->
							<li class="caret"></li>
		
							<!-- Profile sub-links -->
							<li>
								<a href="landing_page.php">
									<i class="fa fa-adn"></i>
									Home
								</a>
							</li>

							<li>
								<a href="#">
									<i class="entypo-user"></i>
									View/Edit Specialist
								</a>
							</li>
		
							<li>
								<a href="#">
									<i class="entypo-google-circles"></i>
									View/Edit Rows
								</a>
							</li>
		
							<li>
								<a href="#">
									<i class="entypo-calendar"></i>
									Leaves and Meetings
								</a>
							</li>
						</ul>
					</li>
		
				</ul>

			</div>
		
		
			<!-- Raw Links -->
			<div class="col-md-6 col-sm-4 clearfix hidden-xs">
		
				<ul class="list-inline links-list pull-right">		
					<li>
						<a href="../scripts/logout.php">
							Log Out <i class="entypo-logout right"></i>
						</a>
					</li>
				</ul>
		
			</div>
		
		</div>
		
		<hr />
		
		<div class="row">
			<div class="col-md-12">
				
				<!-- BODY -->

				<div class="panel-heading">
						<div class="panel-title"><strong>Specialist</strong></div>
						
						<div class="panel-options">
							<ul class="nav nav-tabs">
								<li class="active">
									<a href="#tab1" data-toggle="tab"><i class="entypo-lock">View</i></a>
								</li>
								
								<li>
									<a href="#tab2" data-toggle="tab"><i class="entypo-users">Add</i></a>
								</li>

								<li>
									<a href="#tab3" data-toggle="tab"><i class="entypo-pencil">Edit</i></a>
								</li>
							</ul>
						</div>
					</div>

				<div class="panel-body">
						
						<div class="tab-content">
							<div class="tab-pane active" id="tab1">
								<div class="panel panel-primary">
									<div class="panel-heading">
										<div class="panel-title">View Specialist</div>
										
										<div class="panel-options">
											<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
											<a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
										</div>
									</div>
										
									<table class="table table-bordered table-responsive">
										<thead>
											<tr>
												<th>ID</th>
												<th>Region</th>
												<th>Team</th>
												<th>Specialist</th>
												<th>TNumber</th>
												<th>ShortName</th>
												<th>Email</th>
												<th>Mobile Number</th>
												<th>Role</th>
												<th>Rows</th>
											</tr>
										</thead>
										
										<tbody>
											<?php 

											$selectSQL = mysqli_query($conn, "SELECT specialist.id as ID, specialist.specialist_name as Specialist, support_level.level as Level, team.team_name as Team,region.region_name as Region,specialist.short_name as ShortName, specialist.tnumber as TNumber, specialist.email as Email, specialist.mobile_number as Mobile, support_role.role as Role from specialist_details left join specialist on specialist.ID = specialist_details.specialist_id left join team on specialist_details.team_id = team.ID left join support_level on specialist_details.level_id = support_level.ID left join region on specialist_details.region_id = region.ID left join support_role on specialist_details.role_id = support_role.ID");

											while($row = mysqli_fetch_array($selectSQL))
											{
												?>

												<td><?php echo $row['ID'];?></td>
												<td><?php echo $row['Region'];?></td>
												<td><?php echo $row['Team'];?></td>
												<td><?php echo $row['Specialist'];?></td>
												<td><?php echo $row['ShortName'];?></td>
												<td><?php echo $row['TNumber'];?></td>
												<td><?php echo $row['Email'];?></td>
												<td><?php echo $row['Mobile'];?></td>
												<td><?php echo $row['Role'];?></td>
												<td></td>
												<?php

											}

											?>

										</tbody>
									</table>
								</div>
				
								
							</div>
							
							<div class="tab-pane" id="tab2">
									<div class="tab-content">
										<div class="tab-pane active" id="tab1">
											
											<div class="panel panel-primary">
												<div class="panel-heading">
													<div class="panel-title">View Specialist</div>
													
													<div class="panel-options">
														<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
														<a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
													</div>
												</div>
												<div class="panel-body">
													<div class="col-md-6">
														<div class="form-group">
															Specialist Name
															<input type="text" class="form-control input-sm" name="specialist_name" id="specialist_name" placeholder="Name">
														</div>
														<div class="form-group">
															TNumber
															<input type="text" class="form-control input-sm" name="tnumber" id="tnumber" placeholder="TNumber">
														</div>
														<div class="form-group">
															P&G Short Name
															<input type="text" class="form-control input-sm" name="shortname" id="shortname" placeholder="Short Name">
														</div>	
														<div class="form-group">
															Mobile
															<input type="text" class="form-control input-sm" name="mobile" id="mobile" placeholder="Email">
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															Region
															<select class="form-control" id="region">
																<?php 

																$regionSelectSQL = mysqli_query($conn, "SELECT * from region");

																while($rowRegion = mysqli_fetch_array($regionSelectSQL))
																{
																?>
																	<option value="<?php echo $rowRegion['ID']; ?>"><?php echo $rowRegion['region_name'];?></option>
																<?php }?>
															</select>
														</div>

														<div class="form-group">
															Level
															<select class="form-control" id="region">
																<?php 

																$levelSelectSQL = mysqli_query($conn, "SELECT * from support_level");

																while($levelRegion = mysqli_fetch_array($levelSelectSQL))
																{
																?>
																	<option value="<?php echo $levelRegion['ID']; ?>"><?php echo $levelRegion['level'];?></option>
																<?php }?>
															</select>
														</div>
													</div>
												</div>
												<div class="panel-footer">
													Add New Specialist
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="tab-pane" id="tab3">
								<div class="scrollable" data-height="150" data-autohide="0">
									
								</div>
							</div>
						</div>
						
					</div>

				<!-- BODY -->

			</div>
			
			
			
		</div>
		<!-- Footer -->
		<footer class="main">
			
			&copy; 2017 - Auto<strong>DM</strong> by <a href="#">FSS L1</a>
		
		</footer>
	</div>

		
	




	<!-- Bottom scripts (common) -->
	<script src="../assets/js/gsap/main-gsap.js"></script>
	<script src="../assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="../assets/js/bootstrap.js"></script>
	<script src="../assets/js/joinable.js"></script>
	<script src="../assets/js/resizeable.js"></script>
	<script src="../assets/js/neon-api.js"></script>


	<!-- Imported scripts on this page -->
	<script src="../assets/js/tocify/jquery.tocify.min.js"></script>
	<script src="../assets/js/neon-chat.js"></script>


	<!-- JavaScripts initializations and stuff -->
	<script src="../assets/js/neon-custom.js"></script>


	<!-- Demo Settings -->
	<script src="../assets/js/neon-demo.js"></script>

</body>
</html>
<?php 

  require("../../scripts/connect.php");

?>

<html>
<head>
<!-- IFRAME CSS/JS STYLES -->

	<link rel="stylesheet" href="../../assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="../../assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="../../assets/css/font-icons/font-awesome/css/font-awesome.css">
	<link rel="stylesheet" href="../../assets/css/bootstrap.css">
	<link rel="stylesheet" href="../../assets/css/neon-core.css">
	<link rel="stylesheet" href="../../assets/css/neon-theme.css">
	<link rel="stylesheet" href="../../assets/css/neon-forms.css">
	<link rel="stylesheet" href="../../assets/css/custom.css">
	<script src="../../assets/js/jquery-1.11.0.min.js"></script>

<!-- IFRAME CSS/JS STYLES -->
</head>

<body>

	<form action="../../scripts/add-user.php" method="POST">
		<div class="row">
			<div class="col-md-6">
				<h3><b>Specialist Personal Details</b></h3>
				<hr/>
				<div class="form-group">
					Specialist Name
					<input type="text" class="form-control input-sm" name="specialist_name" id="specialist_name" placeholder="Name">
				</div>
				<div class="form-group">
					TNumber
					<input type="text" class="form-control input-sm" name="tnumber" id="tnumber" placeholder="TNumber">
				</div>
				<div class="form-group">
					P&G Short Name
					<input type="text" class="form-control input-sm" name="shortname" id="shortname" placeholder="Short Name">
				</div>	
				<div class="form-group">
					Mobile
					<input type="text" class="form-control input-sm" name="mobile" id="mobile" placeholder="Mobile">
				</div>
				<div class="form-group">
					Email Address
					<input type="text" class="form-control input-sm" name="email" id="email" placeholder="Email Address">
				</div>
			</div>
			<div class="col-md-6">
				<h3><b>Specialist Team Details</b></h3>
				<hr/>

				<div class="form-group">
					Level
					<select class="form-control" id="team" name="team">
					<?php 

						$teamSelectSQL = mysqli_query($conn, "SELECT * from team");

						while($teamRow = mysqli_fetch_array($teamSelectSQL))
						{
					?>
						<option value="<?php echo $teamRow['ID']; ?>"><?php echo $teamRow['team_name'] . " - " . $teamRow['description'];?></option>
					<?php }?>
					</select>
				</div>

				<div class="form-group">
					Region
					<select class="form-control" id="region" name="region">
						<?php 

							$regionSelectSQL = mysqli_query($conn, "SELECT * from region");

							while($rowRegion = mysqli_fetch_array($regionSelectSQL))
							{
						?>
						<option value="<?php echo $rowRegion['ID']; ?>"><?php echo $rowRegion['region_name'] . " - " . $rowRegion['description'];?></option>
						<?php }?>
					</select>
				</div>

				<div class="form-group">
					Level
					<select class="form-control" id="level" name="level">
					<?php 

						$levelSelectSQL = mysqli_query($conn, "SELECT * from support_level");

						while($levelRegion = mysqli_fetch_array($levelSelectSQL))
						{
					?>
						<option value="<?php echo $levelRegion['ID']; ?>"><?php echo $levelRegion['level'] . " - " . $levelRegion['description'];?></option>
					<?php }?>
					</select>
				</div>
				<br/>
			</div>
		</div>
		<div class="row">
			<div class="form-group">
					<input type="submit" class="btn btn-primary btn-block btn-lg" value="ADD">
				</div>
		</div>
	</form>

<!-- IFRAME CSS/JS STYLES-->
<!-- Bottom scripts (common) -->
	<!-- Imported styles on this page -->
  <link rel="stylesheet" href="../../assets/js/datatables/responsive/css/datatables.responsive.css">
  <link rel="stylesheet" href="../../assets/js/select2/select2-bootstrap.css">
  <link rel="stylesheet" href="../../assets/js/select2/select2.css">

  <!-- Bottom scripts (common) -->
  <script src="../../assets/js/gsap/main-gsap.js"></script>
  <script src="../../assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
  <script src="../../assets/js/bootstrap.js"></script>
  <script src="../../assets/js/joinable.js"></script>
  <script src="../../assets/js/resizeable.js"></script>
  <script src="../../assets/js/neon-api.js"></script>
  <script src="../../assets/js/jquery.dataTables.min.js"></script>
  <script src="../../assets/js/datatables/TableTools.min.js"></script>


  <!-- Imported scripts on this page -->
  <script src="../../assets/js/dataTables.bootstrap.js"></script>
  <script src="../../assets/js/datatables/jquery.dataTables.columnFilter.js"></script>
  <script src="../../assets/js/datatables/lodash.min.js"></script>
  <script src="../../assets/js/datatables/responsive/js/datatables.responsive.js"></script>
  <script src="../../assets/js/select2/select2.min.js"></script>
  <script src="../../assets/js/neon-chat.js"></script>


  <!-- JavaScripts initializations and stuff -->
  <script src="../../assets/js/neon-custom.js"></script>


  <!-- Demo Settings -->
  <script src="../../assets/js/neon-demo.js"></script>
<!-- IFRAME CSS/JS STYLES-->
</body>
</html>
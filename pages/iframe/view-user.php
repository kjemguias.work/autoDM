<?php 

  require("../../scripts/connect.php");

?>

<html>
<head>
<!-- IFRAME CSS/JS STYLES -->

	<link rel="stylesheet" href="../../assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="../../assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="../../assets/css/font-icons/font-awesome/css/font-awesome.css">
  <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="../../assets/css/bootstrap.css">
	<link rel="stylesheet" href="../../assets/css/neon-core.css">
	<link rel="stylesheet" href="../../assets/css/neon-theme.css">
	<link rel="stylesheet" href="../../assets/css/neon-forms.css">
	<link rel="stylesheet" href="../../assets/css/custom.css">
	<script src="../../assets/js/jquery-1.11.0.min.js"></script>


  <style>

    td, th{
      font-size: 12px;
    }

  </style>

<!-- IFRAME CSS/JS STYLES -->
</head>

<body>
                  <h3>Specialist List</h3>
                  <hr/>
                  <table class="table table-bordered datatable" id="table-1">
                      <thead>
                        <tr class="replace-inputs">
                          <th>ID</th>
                          <th>Region</th>
                          <th>Team</th>
                          <th>Specialist</th>
                          <th>TNumber</th>
                          <th>ShortName</th>
                          <th>Email</th>
                          <th>Mobile Number</th>
                          <th>Role</th>
                          <th>Rows</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 

                          $selectSQL = mysqli_query($conn, "SELECT specialist.id as ID, specialist.specialist_name as Specialist, support_level.level as Level, team.team_name as Team,region.region_name as Region,specialist.short_name as ShortName, specialist.tnumber as TNumber, specialist.email as Email, specialist.mobile_number as Mobile, support_role.role as Role from specialist_details left join specialist on specialist.ID = specialist_details.specialist_id left join team on specialist_details.team_id = team.ID left join support_level on specialist_details.level_id = support_level.ID left join region on specialist_details.region_id = region.ID left join support_role on specialist_details.role_id = support_role.ID");

                          while($row = mysqli_fetch_array($selectSQL))
                          {
                            ?>
                            <tr>
                              <td><?php echo $row['ID'];?></td>
                              <td><?php echo $row['Region'];?></td>
                              <td><?php echo $row['Level'] . " " . $row['Team'];?></td>
                              <td><?php echo $row['Specialist'];?></td>
                              <td><?php echo $row['ShortName'];?></td>
                              <td><?php echo $row['TNumber'];?></td>
                              <td><?php echo $row['Email'];?></td>
                              <td><?php 
                              if($row['Mobile'] == '0')
                              {
                                echo "Not set";
                              }
                              else
                                echo $row['Mobile'];

                              ?></td>
                              <td><?php echo $row['Role'];?></td>
                              <td><?php

                              $spec_id = $row['ID'];

                              $selectRowSQL = mysqli_query($conn, "SELECT specialist.ID, specialist.specialist_name, support_rows.row FROM rows_details left join specialist on specialist.ID = rows_details.specialist_id left join support_rows on support_rows.ID = rows_details.row_id where rows_details.specialist_id = '$spec_id'");

                                while($row2 = mysqli_fetch_array($selectRowSQL))
                                {
                                echo $row2["row"] . " ";
                                 } ?>
                              </td>
                            </tr>
                            <?php

                          }

                          ?>
                      </tbody>
                      <tfoot>
                        <tr>
                          <th>ID</th>
                          <th>Region</th>
                          <th>Team</th>
                          <th>Specialist</th>
                          <th>TNumber</th>
                          <th>ShortName</th>
                          <th>Email</th>
                          <th>Mobile Number</th>
                          <th>Role</th>
                          <th>Rows</th>
                        </tr>
                      </tfoot>
                    </table>
                    
<!-- IFRAME CSS/JS STYLES-->
<!-- Bottom scripts (common) -->
	<!-- Imported styles on this page -->
  <script src="../../scripts/table-script.js"></script>
  <link rel="stylesheet" href="../../assets/js/datatables/responsive/css/datatables.responsive.css">
  <link rel="stylesheet" href="../../assets/js/select2/select2-bootstrap.css">
  <link rel="stylesheet" href="../../assets/js/select2/select2.css">

  <!-- Bottom scripts (common) -->
  <script src="../../assets/js/gsap/main-gsap.js"></script>
  <script src="../../assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
  <script src="../../assets/js/bootstrap.js"></script>
  <script src="../../assets/js/joinable.js"></script>
  <script src="../../assets/js/resizeable.js"></script>
  <script src="../../assets/js/neon-api.js"></script>
  <script src="../../assets/js/jquery.dataTables.min.js"></script>
  <script src="../../assets/js/datatables/TableTools.min.js"></script>


  <!-- Imported scripts on this page -->
  <script src="../../assets/js/dataTables.bootstrap.js"></script>
  <script src="../../assets/js/datatables/jquery.dataTables.columnFilter.js"></script>
  <script src="../../assets/js/datatables/lodash.min.js"></script>
  <script src="../../assets/js/datatables/responsive/js/datatables.responsive.js"></script>
  <script src="../../assets/js/select2/select2.min.js"></script>
  <script src="../../assets/js/neon-chat.js"></script>


  <!-- JavaScripts initializations and stuff -->
  <script src="../../assets/js/neon-custom.js"></script>


  <!-- Demo Settings -->
  <script src="../../assets/js/neon-demo.js"></script>
<!-- IFRAME CSS/JS STYLES-->
</body>
</html>
<?php 

  require("../../scripts/connect.php");

?>

<html>
<head>
<!-- IFRAME CSS/JS STYLES -->

	<link rel="stylesheet" href="../../assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="../../assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="../../assets/css/font-icons/font-awesome/css/font-awesome.css">
  <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="../../assets/css/bootstrap.css">
	<link rel="stylesheet" href="../../assets/css/neon-core.css">
	<link rel="stylesheet" href="../../assets/css/neon-theme.css">
	<link rel="stylesheet" href="../../assets/css/neon-forms.css">
	<link rel="stylesheet" href="../../assets/css/custom.css">
	<script src="../../assets/js/jquery-1.11.0.min.js"></script>


  <style>

    td, th{
      font-size: 12px;
    }

  </style>

<!-- IFRAME CSS/JS STYLES -->
</head>

<body>
 
 <div class="alert alert-Danger"><strong>Error!</strong> An error has occured upon adding entries, please check the details provided.</div>

<!-- IFRAME CSS/JS STYLES-->
<!-- Bottom scripts (common) -->
	<!-- Imported styles on this page -->
  <link rel="stylesheet" href="../../assets/js/datatables/responsive/css/datatables.responsive.css">
  <link rel="stylesheet" href="../../assets/js/select2/select2-bootstrap.css">
  <link rel="stylesheet" href="../../assets/js/select2/select2.css">

  <!-- Bottom scripts (common) -->
  <script src="../../assets/js/gsap/main-gsap.js"></script>
  <script src="../../assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
  <script src="../../assets/js/bootstrap.js"></script>
  <script src="../../assets/js/joinable.js"></script>
  <script src="../../assets/js/resizeable.js"></script>
  <script src="../../assets/js/neon-api.js"></script>
  <script src="../../assets/js/jquery.dataTables.min.js"></script>
  <script src="../../assets/js/datatables/TableTools.min.js"></script>


  <!-- Imported scripts on this page -->
  <script src="../../assets/js/dataTables.bootstrap.js"></script>
  <script src="../../assets/js/datatables/jquery.dataTables.columnFilter.js"></script>
  <script src="../../assets/js/datatables/lodash.min.js"></script>
  <script src="../../assets/js/datatables/responsive/js/datatables.responsive.js"></script>
  <script src="../../assets/js/select2/select2.min.js"></script>
  <script src="../../assets/js/neon-chat.js"></script>


  <!-- JavaScripts initializations and stuff -->
  <script src="../../assets/js/neon-custom.js"></script>


  <!-- Demo Settings -->
  <script src="../../assets/js/neon-demo.js"></script>
<!-- IFRAME CSS/JS STYLES-->
</body>
</html>
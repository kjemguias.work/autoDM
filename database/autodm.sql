-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 25, 2017 at 10:22 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `autodm`
--

-- --------------------------------------------------------

--
-- Table structure for table `region`
--

CREATE TABLE IF NOT EXISTS `region` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `region_name` varchar(50) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `region`
--

INSERT INTO `region` (`ID`, `region_name`, `description`) VALUES
(1, 'ASIA', 'ASIA support (morning shift)'),
(2, 'EMEA', 'EMEA support (midshift)'),
(3, 'AMER', 'AMER support (night-graveyard shift)');

-- --------------------------------------------------------

--
-- Table structure for table `rows_details`
--

CREATE TABLE IF NOT EXISTS `rows_details` (
  `specialist_id` int(11) NOT NULL,
  `row_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `specialist`
--

CREATE TABLE IF NOT EXISTS `specialist` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `specialist_name` varchar(100) NOT NULL,
  `short_name` varchar(50) NOT NULL,
  `tnumber` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mobile_number` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `specialist`
--

INSERT INTO `specialist` (`ID`, `specialist_name`, `short_name`, `tnumber`, `email`, `mobile_number`) VALUES
(1, 'Kirt Guias', 'guias.k', 'CZ6054', 'kirt.guias@dxc.com', '+639776270158'),
(2, 'Michelle Pelobillo', 'pelobillo.M', 'CZ6055', 'michelle.pelobillo@dxc.com', 'not set'),
(3, 'Carranzo, Gil', 'carranzo.g', 'CY7376', 'gil.carranzo@dxc.com', 'not set'),
(4, 'De Leon, Carlos', 'deleon.c.2', 'DB9363', 'carlos.de.leon@dxc.com', 'not set');

-- --------------------------------------------------------

--
-- Table structure for table `specialist_details`
--

CREATE TABLE IF NOT EXISTS `specialist_details` (
  `specialist_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  UNIQUE KEY `specialist_id` (`specialist_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `specialist_details`
--

INSERT INTO `specialist_details` (`specialist_id`, `team_id`, `region_id`, `level_id`, `role_id`) VALUES
(1, 3, 2, 1, 0),
(2, 3, 1, 1, 1),
(3, 3, 2, 1, 2),
(4, 1, 2, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `specialist_leave`
--

CREATE TABLE IF NOT EXISTS `specialist_leave` (
  `specialist_id` int(11) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `specialist_leave`
--

INSERT INTO `specialist_leave` (`specialist_id`, `description`) VALUES
(2, 'gutom lang');

-- --------------------------------------------------------

--
-- Table structure for table `support_level`
--

CREATE TABLE IF NOT EXISTS `support_level` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `level` varchar(50) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `support_level`
--

INSERT INTO `support_level` (`ID`, `level`, `description`) VALUES
(1, 'L1', 'FSS Level 1 Support'),
(2, 'L2', 'FSS Level 2 Support'),
(3, 'L3', 'FSS Level 3 Support');

-- --------------------------------------------------------

--
-- Table structure for table `support_role`
--

CREATE TABLE IF NOT EXISTS `support_role` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(50) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `support_role`
--

INSERT INTO `support_role` (`ID`, `role`, `description`) VALUES
(1, 'DM1', 'Duty Manager 1 (for L1)'),
(2, 'DM2', 'Duty Manager 2 (for L1)'),
(3, 'SM', 'Shift Manager (for L2)');

-- --------------------------------------------------------

--
-- Table structure for table `support_rows`
--

CREATE TABLE IF NOT EXISTS `support_rows` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `row` varchar(50) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `support_rows`
--

INSERT INTO `support_rows` (`ID`, `row`, `description`) VALUES
(1, 'AP', 'Accounts Payable'),
(2, 'TR', 'Treasury'),
(3, 'AR', 'Accounts Receivable'),
(4, 'TE', ''),
(5, 'GL', 'General Ledger'),
(6, 'CO', 'Cost Accounting'),
(7, 'AA', 'Asset Accounting'),
(8, 'MS', ''),
(9, 'PS', ''),
(10, 'PA', ''),
(11, 'PA', ''),
(12, 'AI', '');

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE IF NOT EXISTS `team` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `team_name` varchar(50) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`ID`, `team_name`, `description`) VALUES
(1, 'PBS', 'PBS (Payments and Banking Services)'),
(2, 'MEC', 'MEC (Month end closing)'),
(3, 'PBS / MEC', 'L1 for PBS and MEC');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

    var frm = $('#rowForm');

    frm.submit(function (e) {

        e.preventDefault();
        var formData = {
            'row'           : $("input#rowName").val(),
            'description'   : $("input#description").val(),
        };

        $.ajax({
            type: 'POST',
            url: 'add-row.php',
            data: formData,
            dataType: 'json'
        })

        .done(function(data) {

            alert(data);


        });
    });

<?php 
	require("connect.php");


	$specialist_name = mysqli_real_escape_string($conn,$_POST['specialist_name']); 
	$specialist_tnumber = mysqli_real_escape_string($conn,$_POST['tnumber']); 
	$specialist_shortname = mysqli_real_escape_string($conn,$_POST['shortname']); 
	$specialist_mobile = mysqli_real_escape_string($conn,$_POST['mobile']); 
	$specialist_email = mysqli_real_escape_string($conn,$_POST['email']); 

	$specialist_team = mysqli_real_escape_string($conn,$_POST['team']); 
	$specialist_region = mysqli_real_escape_string($conn,$_POST['region']); 
	$specialist_level = mysqli_real_escape_string($conn,$_POST['level']); 

	$status = "valid";

	$selectSQL = mysqli_query($conn, "SELECT * from specialist where tnumber = '$specialist_tnumber' OR short_name = '$specialist_shortname'");

	$row = mysqli_fetch_array($selectSQL);

	if(strtolower($specialist_tnumber) == strtolower($row['tnumber']))
	{
		$status = "invalid";
	}

	if(strtolower($specialist_shortname) == strtolower($row['short_name']))
	{
		$status = "invalid";
	}

	echo $specialist_team;

	if($status == "valid")
	{

		$insertSQLSpecialist = "INSERT INTO `specialist`(`specialist_name`, `short_name`, `tnumber`, `email`, `mobile_number`) VALUES ('$specialist_name','$specialist_shortname','$specialist_tnumber','$specialist_email','$specialist_mobile')";

		if (mysqli_query($conn, $insertSQLSpecialist)) {
		    $last_id = mysqli_insert_id($conn);

		    $insertSQLSpecialist_details = mysqli_query($conn, "INSERT INTO `specialist_details`(`specialist_id`, `team_id`, `region_id`, `level_id`, `role_id`) VALUES ('$last_id','$specialist_team', '$specialist_region','$specialist_level','0')");
		    header("Location: ../pages/prompt/added-successful.php");
		} else {
		    header("Location: ../pages/prompt/added-fail.php");
		}
	}
	else
	{
		header("Location: ../pages/prompt/added-fail2.php");
	}

?>

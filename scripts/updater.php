<?php 

require("connect.php");

// Fields Submitted
$ticketNum = mysqli_real_escape_string($conn,$_POST['ticketNumber']); 
$severity = mysqli_real_escape_string($conn,$_POST['severity']);
$closureCode = mysqli_real_escape_string($conn,$_POST['closureCode']); 
$workNotes = mysqli_real_escape_string($conn,$_POST['workNotes']); 
$rootCause = mysqli_real_escape_string($conn,$_POST['rootCause']); 
$resolution = mysqli_real_escape_string($conn,$_POST['resolution']); 

$arr = array_fill(0,6,'');

ini_set('max_execution_time', -1);

$resArr = $resolution;

$resolution =str_replace('\r\n',' ',$resolution);

$textAr = explode('\r\n', $ticketNum);

$textAr = array_filter($textAr, 'trim');

$arr = explode('\r\n', $resArr);

//array_push($arr,'','','','','', '');

var_dump($textAr);
var_dump($arr);

for($i = 0; $i < count($textAr); $i++)
{

	$iim1 = new COM("imacros");
	$s = $iim1->iimOpen("-runner", false, 40);
	$s = $iim1->iimSet("ticketNumber","$textAr[$i]");
	$s = $iim1->iimPlay("C:\wamp\www\autoDM2\macros\searchIM.iim");
	$s = $iim1->iimSet("ticketNumber","$textAr[$i]");
	//$s = $iim1->iimSet("severity"."$severity");
		/*
	$s = $iim1->iimSet("closureCode", "$closureCode");
	$s = $iim1->iimSet("workNotes","$workNotes");
	$s = $iim1->iimSet("rootCause","$rootCause");
	$s = $iim1->iimSet("resolution","$resolution");
	$s = $iim1->iimSet("first","$arr[0]");
	$s = $iim1->iimSet("second","$arr[1]");
	$s = $iim1->iimSet("third","$arr[2]");
	$s = $iim1->iimSet("fourth","$arr[3]");
	$s = $iim1->iimSet("fifth","$arr[4]");
	$s = $iim1->iimSet("sixth","$arr[5]");
	$s = $iim1->iimSet("seventh","$arr[6]");
	$s = $iim1->iimPlay("C:\wamp\www\autoDM2\macros\closeTicket.iim");
	if($s != '1')
	{
		echo $s;
		echo "<script type='text/javascript'>alert('there was an error upon searching the ticket, please check ticketNumber');</script>";
	}*/

	$s = iMacro2($textAr[$i], $severity, $workNotes, $rootCause, $closureCode,$arr, $iim1);
	if($s)
		header("Location: ../pages/landing_page.php?success=1");
	if($s != 1)
		header("Location: ../pages/landing_page.php?success=0");
}

function iMacro2($ticketNumber, $severity, $workNotes, $rootCause, $closureCode, $arr, $iim1)
{
	$severity = str_replace(' ', '<SP>', $severity);
	$workNotes = str_replace(' ','<SP>', $workNotes);
	$rootCause = str_replace(' ','<SP>', $rootCause);
	$closureCode =str_replace(' ','<SP>',$closureCode);

	$p = "VERSION BUILD=11.5.499.3066" . "\n";
	$p .= "TAB T=1" . "\n";
	$p .= "TAB CLOSEALLOTHERS" . "\n";
	$p .= "SET !PLAYBACKDELAY 0.2" . "\n";
	$p .= "WAIT SECONDS=3"  . "\n";
	$p .= "FRAME NAME=gsft_main"  . "\n";
	$p .= "TAG POS=1 TYPE=A ATTR=TXT:". $ticketNumber  . "\n";
	$p .= "WAIT SECONDS=2"  . "\n";
	$p .= "ONDIALOG POS=1 BUTTON=NO"  . "\n";
	$p .= "TAG POS=1 TYPE=BUTTON:SUBMIT ATTR=ID:resolve_incident"  . "\n";
	$p .= "TAG POS=1 TYPE=SELECT ATTR=NAME:incident.severity CONTENT=$" . $severity . "\n";
	$p .= "TAG POS=1 TYPE=SPAN ATTR=TXT:Work<SP>Notes"  . "\n";
	$p .= "TAG POS=1 TYPE=TEXTAREA ATTR=NAME:incident.comments CONTENT=" . $workNotes  . "\n";
	$p .= "TAG POS=1 TYPE=SPAN ATTR=TXT:Closure<SP>Information"  . "\n";
	$p .= "TAG POS=1 TYPE=TEXTAREA ATTR=NAME:incident.u_string_3 CONTENT=" . $rootCause  . "\n";
	$p .= "TAG POS=1 TYPE=TEXTAREA ATTR=NAME:incident.close_notes CONTENT=";
	for($i = 0; $i < count($arr); $i++)
	{
		$arr[$i] = str_replace(' ','<SP>',$arr[$i]);
		$p .= $arr[$i] . "<BR><LF>";
	}
	$p .= "\n";
	$p .= "TAG POS=1 TYPE=SELECT ATTR=NAME:incident.close_code CONTENT=$" . $closureCode  . "\n";
	$p .= "TAG POS=1 TYPE=DIV ATTR=ID:element.incident.close_notes" . "\n";
	$p .= "TAG POS=2 TYPE=BUTTON:SUBMIT ATTR=NAME:not_important" . "\n";
	$p .= "WAIT SECONDS=2" . "\n";
	$p .= "TAG POS=3 TYPE=BUTTON:SUBMIT ATTR=NAME:not_important" . "\n";
	$p .= "WAIT SECONDS=2" . "\n";

	$s = $iim1->iimPlayCode($p);

	echo $p;

	return $s;
}

function iMacro($ticketNumber, $workNotes, $rootCause, $resolution, $arr, $i)
{
	
	$iim1 = new COM("imacros");
	$s = $iim1->iimOpen("-runner", false, 40);
	$s = $iim1->iimSet("ticketNumber","$ticketNumber[$i]");
	$s = $iim1->iimPlay("C:\wamp\www\autoDM2\macros\searchIM.iim");
	if($s != '1')
	{
		echo $s;
		echo "<script type='text/javascript'>alert('there was an error upon searching the ticket, please check ticketNumber');</script>";

		return 0;
	}
	$iim1 = new COM("imacros");
	$s = $iim1->iimOpen("-runner", false);
	$s = $iim1->iimSet("ticketNumber","$ticketNumber[$i]");
	$s = $iim1->iimSet("workNotes","$workNotes");
	$s = $iim1->iimSet("rootCause","$rootCause");
	$s = $iim1->iimSet("resolution","$resolution");
	$s = $iim1->iimSet("first","$arr[0]");
	$s = $iim1->iimSet("second","$arr[1]");
	$s = $iim1->iimSet("third","$arr[2]");
	$s = $iim1->iimSet("fourth","$arr[3]");
	$s = $iim1->iimSet("fifth","$arr[4]");
	$s = $iim1->iimSet("sixth","$arr[5]");
	$s = $iim1->iimPlay("C:\wamp\www\autoDM2\macros\closeTicket.iim");
	//sleep(40);
	if($s != '1')
	{
		echo "<script type='text/javascript>a'lert('an error was occured');</script>";
		return 0;
	}
	else
	{
		if($ticketNumber[$i + 1] == 'empty')
		{
			return 1;
		}
		else
		{
			$r = iMacro($ticketNumber, $workNotes, $rootCause, $resolution, $arr, $i + 1);
		}
	}
}
?>